<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        $userRoles = Auth::user()->roles->pluck('name');
        if( !$userRoles->contains('admin')){
            return redirect()->back()->withErrors("Sorry! You don't have the rights to access this page.");
        }
        return $next($request);
    }
}
