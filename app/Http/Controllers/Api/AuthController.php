<?php

namespace App\Http\Controllers\Api;

use Auth;
use Hash;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
	/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){ 

    	$request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|min:6',
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) 
        {
        	return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('MyDService Personal Access Token');
        $token = $tokenResult->token;

        $token->save();

        $response = [ 
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
        ];

        return response($response , 200);

    }

	/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register (Request $request) 
    {

	    $validator = Validator::make($request->all(), [
	        'name' => 'required|string|max:255',
	        'email' => 'required|string|email|max:255|unique:users',
	        'password' => 'required|string|min:6|confirmed',
	    ]);

	    if ($validator->fails())
	    {
	        return response(['errors'=>$validator->errors()->all()], 401);
	    }

	    $request['password']=Hash::make($request['password']);
	    $user = User::create($request->toArray());

	    $token = $user->createToken('MyDService Personal Access Token')->accessToken;
	    $response = ['token' => $token];

	    return response($response, 200);

	}

	/**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
	public function logout (Request $request) 
	{
	    $request->user()->token()->revoke();

	    $response = 'You have been succesfully logged out!';
	    return response($response, 200);
	}


	public function user(Request $request)
    {
        return response($request->user());
    }
}
