<?php

namespace App\Http\Controllers\Api;

use App\Point;
use App\Http\Resources\PointResource;

use App\Http\Middleware\IsAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointController extends Controller
{

    public function __construct()
    {
        $this->middleware(IsAdmin::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PointResource::collection(Point::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'   => 'required|string|unique:points,name',
        );

        // Validate data
        $validator = Validator::make( $request->all(), $rules );
        if ( $validator->fails() ) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $point = new Point();
        $point->name   = $request->get('point');
        $point->save();
        $response = ['message'=> 'Success adding new Point'];
        return response([$response]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $point = Point::find($id);
        return response([$point]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $point = Point::find($id);
        $rules = array(
            'name'   => 'required|string',
        );
        // Validate data
        $validator = Validator::make( $request->all(), $rules );
        if ( $validator->fails() ) {
            return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        $point->name   = $request->get('point');
        $point->save();
        $response = ['message'=> 'Success update Point'];
        return response([$response]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $point = Point::find($id);
        $point->delete();
        return response('Deleted Successfully');
    }
}
