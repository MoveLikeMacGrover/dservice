<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->middleware(['json.response'])->group(function () {
    // public routes
    Route::post('login', 'AuthController@login')->name('api.login');
    Route::post('register', 'AuthController@register')->name('api.register');

    // private routes
    Route::middleware('auth:api')->group(function () {
        Route::get('logout', 'AuthController@logout')->name('api.logout');
        Route::get('user', 'AuthController@user');
        Route::middleware('isadmin')->group(function () {
            Route::apiResource('points', 'PointController');
            Route::apiResource('routes', 'RouteController');
        });
    });

    Route::get('path', 'PathController@index');
    


});


