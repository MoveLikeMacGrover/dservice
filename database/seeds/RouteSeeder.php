<?php

use App\Route;
use Carbon\Carbon;

use Illuminate\Database\Seeder;

class RouteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Route::create(['origin' => 'A','destination' => 'C','time' => '1','cost' => '20']);
        Route::create(['origin' => 'A','destination' => 'H','time' => '10','cost' => '1']);
        Route::create(['origin' => 'A','destination' => 'E','time' => '30','cost' => '5']);
        Route::create(['origin' => 'C','destination' => 'B','time' => '1','cost' => '12']);
        Route::create(['origin' => 'H','destination' => 'E','time' => '30','cost' => '1']);
        Route::create(['origin' => 'E','destination' => 'D','time' => '3','cost' => '5']);
        Route::create(['origin' => 'D','destination' => 'F','time' => '4','cost' => '50']);
        Route::create(['origin' => 'F','destination' => 'I','time' => '45','cost' => '50']);
        Route::create(['origin' => 'F','destination' => 'G','time' => '40','cost' => '50']);
        Route::create(['origin' => 'I','destination' => 'B','time' => '65','cost' => '5']);
        Route::create(['origin' => 'G','destination' => 'B','time' => '64','cost' => '73']);
    }
}
