<?php

use Illuminate\Database\Seeder;

use App\Role;
class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'ADMIN']);
        $partner = Role::create(['name' => 'PARTNER']);
    }
}
