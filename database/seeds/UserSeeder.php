<?php

use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(Faker $faker) {
		// create admin
		$admin = User::create([
			'name' => 'Admin',
			'email' => 'admin@mail.com',
			'email_verified_at' => now(),
			'password' => Hash::make('secret'),
		]);
		$admin->roles()->attach(1);

		// create other admin user
		$admin_user = factory(App\User::class, 2)->create();
		foreach ($admin_user as $key => $user) {
			$user->roles()->attach(1);
		}

		// create partner users
		$partner_user = factory(App\User::class, 7)->create();
		foreach ($partner_user as $key => $user) {
			$user->roles()->attach(2);
		}

	}
}
