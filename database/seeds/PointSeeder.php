<?php

use App\Point;
use Carbon\Carbon;

use Illuminate\Database\Seeder;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Point::create(['name' => 'A']);
        Point::create(['name' => 'B']);
        Point::create(['name' => 'C']);
        Point::create(['name' => 'D']);
        Point::create(['name' => 'E']);
        Point::create(['name' => 'F']);
        Point::create(['name' => 'G']);
        Point::create(['name' => 'H']);
        Point::create(['name' => 'I']);
    }
}
